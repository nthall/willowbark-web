+++
+++

{{< flexsection title="Services" class="columns-section" >}}
{{< div >}}
### Build Websites

Lorem ipsum dolor sit amet,
consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore,
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui foficia deserunt mollit anim id est
laborum.
{{< /div >}}
{{< div >}}
### Host Stuff

Lorem ipsum dolor sit amet,
consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore,
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui foficia deserunt mollit anim id est
laborum.
{{< /div >}}
{{< div >}}
### Develop Tools

Lorem ipsum dolor sit amet,
consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore,
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui foficia deserunt mollit anim id est
laborum.
{{< /div >}}
{{< /flexsection >}}

{{< section title="Values" class="list-section" >}}

### Democratic egalitarian member control.

 Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor
 indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis
 nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat.
 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
 fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
 culpa qui foficia deserunt mollit anim id est laborum. 

### Accountable to impacted communities.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### Privacy, decentralization, & autonomy.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### The Commons.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### Sustainable economics and ecology.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### Some other stuff.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### This section can't fit on a page.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### So we can see what scrolling is like.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### On my monitor anyways.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

### It just keeps going.

Lorem ipsum dolor sit amet, consecutur adipscing elit, sed do eiusmod tempor indicidunt ut labore, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamo laboris nisi ut aliqujip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui foficia deserunt mollit anim id est laborum.

{{< /section >}}

{{< section title="Contact" anchor="contact" class="minimal-section" >}}
We'd love to work with you, why not drop us an email at [hello@example.net](mailto:hello@example.net)
{{< /section >}}
